# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'ski'
set :repo_url, 'git@bitbucket.org:12deg/ski.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :branch, 'master'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/kunden/493443_20354/webseiten/ski'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :airbrussh.
set :format, :airbrussh

# Default value for :log_level is :debug
set :log_level, :debug

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# Link file settings.php
set :linked_files, fetch(:linked_files, []).push('sites/default/settings.php')
set :linked_files, fetch(:linked_files, []).push('sites/default/default.settings.php')
set :linked_files, fetch(:linked_files, []).push('sites/default/default.services.yml')
set :linked_files, fetch(:linked_files, []).push('sites/default/services.yml')


# Link dirs files and private-files
set :linked_dirs, fetch(:linked_dirs, []).push('sites/default/files', 'private-files')

# We don't use an app path
set :app_path, ""

invoke :production

SSHKit.config.command_map[:composer] = "#{shared_path.join("/usr/local/bin/composer")}"
SSHKit.config.command_map[:drush] = "#{shared_path.join("vendor/bin/drush")}"

set :composer_install_flags, '--no-dev --prefer-dist --no-interaction --optimize-autoloader'

# for this to work you need to patch your local gem with https://github.com/capistrano/drupal-deploy/pull/10
set :drush_version, "~8.0.0"

namespace :deploy do

  desc "Set file system variables"
  task :after_deploy_updated do
      #invoke "drupal:dumpdb"
      #invoke "drupal:update:updatedb"
      #invoke "drupal:feature_revert"
      #invoke "drupal:cache:clear"
  end

  #before 'deploy:symlink:release', "drupal:site_offline"
  #after 'deploy:symlink:release', "deploy:after_deploy_updated"

  #after :finished, "drupal:site_online"

end

namespace :drupal do

    desc "Include creation of additional folder for backups"
    task :prepare_database_backup_path do
        on release_roles :app do
            execute :mkdir, '-p', "#{deploy_to}/database_backups"
            execute :chmod, '750', "#{deploy_to}/database_backups"
        end
    end

    desc "Backup the database"
    task :dumpdb do
        on release_roles :app do
            release_name = Time.now.utc.strftime("%Y%m%d.%H%M%S")
            execute :drush, '-r', "#{deploy_to}/current sql-dump --gzip --result-file=#{deploy_to}/database_backups/#{release_name}.sql.gz"
        end
    end

end
